def solution(arr):

    result = []
    arr.sort()
    r= len(arr) - 1

    for i in xrange(len(arr)-2):
        l = i + 1
        while (l < r):
            sum_ = arr[i] + arr[l] + arr[r]
            if (sum_ < 0):
                l = l + 1
            if (sum_ > 0):
                r = r - 1
            if not sum_:  # 0 is False in a boolean context
                result.append([arr[i], arr[l], arr[r]])
                l = l + 1  # increment l when we find a combination that works

    non_repeats = []
    [non_repeats.append(sublst) for sublst in result if not non_repeats.count(sublst)]

    return non_repeats

print solution([-1,0,1,2,-1,-4])
