
def reverse_bits3(n):
    m = 0
    i = 0
    while i < 32:
        m = (m << 1) + (n & 1)
        n >>= 1
        i += 1
    return m



print reverse_bits3(43261596)


